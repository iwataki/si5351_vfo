/*
 * Button.h
 *
 *  Created on: 2017/04/03
 *      Author: �@��Y
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "Textbox.h"

class Button: public Textbox {
private:

	void drawPushed(void);
	void drawNotPushed(void);
	bool _pushed;
public:
	Button(Adafruit_GFX*graphic_driver,int x,int y,int width,int height,const std::string&str="");
	void draw(void);
	void setState(bool pushed);
	virtual ~Button();
};

#endif /* BUTTON_H_ */
