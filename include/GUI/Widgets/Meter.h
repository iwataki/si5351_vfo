/*
 * Meter.h
 *
 *  Created on: 2017/04/03
 *      Author: 宗一郎
 */

#ifndef METER_H_
#define METER_H_

#include "Widget.h"
/**
 * @brief 一次元グラフの基底クラス，バーグラフやメーターなど
 */
class Meter: public Widget {
protected:
	int _max,_min,_value;
public:
	Meter(Adafruit_GFX*graphic_driver,int x,int y,int width,int height);
	void setMax(int max);
	void setMin(int min);
	void setValue(int val);
	virtual ~Meter();
};

#endif /* METER_H_ */
