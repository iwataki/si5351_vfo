/*
 * ProgressBar.h
 *
 *  Created on: 2017/04/08
 *      Author: 宗一郎
 */

#ifndef PROGRESSBAR_H_
#define PROGRESSBAR_H_

#include "Meter.h"
/**
 * @brief バーグラフ
 */
class ProgressBar: public Meter {
private:
	uint16_t _barColor;
public:
	ProgressBar(Adafruit_GFX*graphic_driver,int x,int y,int width,int height);
	void draw(void);
	void setBarColor(uint16_t color);
	virtual ~ProgressBar();
};

#endif /* PROGRESSBAR_H_ */
