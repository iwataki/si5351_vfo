/*
 * Textbox.h
 *
 *  Created on: 2017/04/03
 *      Author: �@��Y
 */

#ifndef TEXTBOX_H_
#define TEXTBOX_H_

#include "Widget.h"
#include <string>
#include <stdint.h>
class Textbox: public Widget {
private:
	std::string _str;
	bool _wrap;
	uint16_t _txtColor;
public:
	Textbox(Adafruit_GFX*graphic_driver,int x,int y,int width,int height,const std::string&str="");
	virtual void draw(void);
	void setText(const std::string&str);
	void setWrap(bool wrap);
	void setTextColor(uint16_t color);

	virtual ~Textbox();
};

#endif /* TEXTBOX_H_ */
