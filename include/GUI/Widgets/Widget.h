/*
 * Widgets.h
 *
 *  Created on: 2017/04/03
 *      Author: �@��Y
 */

#ifndef WIDGETS_H_
#define WIDGETS_H_
#include "../Adafruit_GFX.h"


class Widget {
public:
	typedef void(*callback_t)(Widget*);
protected:
	int _x,_y,_width,_height;
	Adafruit_GFX*_graphic_device;
	callback_t _cb;
	void*_user_data;
	uint16_t _bgColor;
	uint16_t _upColor,_bottomColor;
	bool _is_changed;
public:

	Widget(Adafruit_GFX*graphic_driver,int x,int y,int width,int height);
	virtual void draw(void)=0;
	void setBackGroundColor(uint16_t);
	void setBoundColor(uint16_t up,uint16_t bottom);
	void callback(callback_t cb,void*user_data=0);
	void erase();
	void remove();
	bool needRedraw(void);
	void needRedraw(bool is_requested);
	virtual ~Widget();
};

#endif /* WIDGETS_H_ */
