/*
 * Window.h
 *
 *  Created on: 2017/04/12
 *      Author: �@��Y
 */

#ifndef GUI_WIDGETS_WINDOW_H_
#define GUI_WIDGETS_WINDOW_H_

#include <GUI/Widgets/Widget.h>
#include "GUI/Adafruit_GFX.h"
#include <vector>

class Window: public Widget {
protected:
	std::vector<Widget*>_Items;
public:
	/**
	 * @brief create subcomponent here
	 * @param dipslay pointer to display device
	 * @param width width
	 * @param height height
	 */
	Window(Adafruit_GFX*display,int width,int height);
	void draw(void);
	virtual ~Window();
};

#endif /* GUI_WIDGETS_WINDOW_H_ */
