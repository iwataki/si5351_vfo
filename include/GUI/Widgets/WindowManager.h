/*
 * WindowManager.h
 *
 *  Created on: 2017/04/05
 *      Author: 宗一郎
 */

#ifndef WINDOWMANAGER_H_
#define WINDOWMANAGER_H_
#include "Widget.h"
#include <vector>
/**
 * @brief widget　管理クラス
 *
 *
 */
class WindowManager {
private:
	std::vector<Widget*>_widgets;
	Adafruit_GFX*_display;
	bool _isrunning;
	volatile bool _issynced;
	void(*_init_func)(void);
	void(*_loop_func)(void);
	WindowManager();
	WindowManager(const WindowManager&)=delete;
	WindowManager& operator=(const WindowManager&)=delete;
	static WindowManager _instance;
	~WindowManager();
public:
	static WindowManager& getInstance(void);
	void addWidget(Widget*);
	void addDisplay(Adafruit_GFX*);
	Adafruit_GFX*getDisplay(void);
	void removeWidget(Widget*);
	void run();
	bool isRunnning(void);
	void registerInit(void(*func)(void));
	void registerLoop(void(*func)(void));
	void syncWait(void);
	void sync(void);

};

#endif /* WINDOWMANAGER_H_ */
