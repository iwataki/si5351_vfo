/*
 * VFOWinfow.h
 *
 *  Created on: 2017/04/12
 *      Author: �@��Y
 */

#ifndef VFOWINDOW_H_
#define VFOWINDOW_H_

#include <GUI/Widgets/Window.h>
#include "GUI/Widgets/Textbox.h"
#include "GUI/Widgets/Button.h"

class VFOWindow: public Window {
private:
	Textbox*_freqencyText=0;//frequency
	Textbox*_frequencyStepText=0;//step:
	Textbox*_ritStatusText=0;//rit ON/OFF
	Textbox*_ritFreqText=0;
	Textbox*_vfoStatusText=0;//vfo A/B
	Textbox*_operationModeText=0;//am/ssb
	Textbox*_xmitStatusText=0;
	Button*_leftBtn=0;//function button
	Button*_centerBtn=0;//rit@func1,vfoa/b@func2
	Button*_rightBtn=0;//step@func1,mode@func2
public:
	VFOWindow(Adafruit_GFX*display);
	void setFreq(int freq);
	void setVfoAB(int ch);
	void setFreqStep(int step);
	void setRitEnable(bool eneble);
	void setRitFocus(bool enable);
	void setRitFreq(int freq);
	void setLeftButton(bool pushed);
	void setRightButton(bool pushed);
	void setCenterButton(bool pushed);
	void setFunction(int function);
	void setModurationMode(int mode);
	void setTransmitStatus(bool txing);
	virtual ~VFOWindow();
};

#endif /* VFOWINDOW_H_ */
