/*
 * ad9834.h
 *
 *  Created on: 2016/10/08
 *      Author: �@��Y
 */

#ifndef AD9834_H_
#define AD9834_H_
#include <stdint.h>
#include "hal/SPI.h"
#include "hal/gpio.h"

#define AD9834REFCLK 48000000

class AD9834 {
private:
	SPI&_Port;
	DigitalOut&_CS;
	DigitalOut&_Reset;
	void sendWord(uint16_t data);
	void reset(void);
public:
	AD9834(SPI&Port,DigitalOut&CS,DigitalOut&Reset);
	void SetFrequency(uint32_t Freq);
};


#endif /* AD9834_H_ */
