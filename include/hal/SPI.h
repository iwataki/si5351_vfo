/*
 * spi.h
 *
 *  Created on: 2014/12/10
 *      Author: �@��Y
 */

#ifndef SPI_H_
#define SPI_H_

#include <stm32f30x_conf.h>

/*! @addtogroup Peripheral*/
/*! @{*/

/**
 *
 * @brief SPI serial comm
 */
class SPI{
public:
	typedef void(*handler_t)(void*);
private:
	SPI_TypeDef*SPIPORT;
	SPI_InitTypeDef spi_config_context;
	int SPI_ch;
	GPIO_TypeDef*GPIOFORSPI;
	int SCKbit;
	int MISObit;
	int MOSIbit;
	static bool SPI1_busy;
	static bool SPI2_busy;
	static bool SPI3_busy;
	handler_t _handler;
	void*_handler_arg;
	uint16_t _latest;
public:

	enum SPI_MODE{Mode0=0x0,Mode1=0x01,Mode2=0x02,Mode3=0x03};
	enum SPI_DIR{Master=SPI_Mode_Master,Slave=SPI_Mode_Slave};
	enum SPI_CLKDIV{div2=0x0,div4=0x08,div8=0x10,div16=0x18,div32=0x20,div64=0x28,div128=0x30,div256=0x38};
	/**
	 * @brief SPI init
	 * @param ch
	 * @param GPIO_SPI
	 * @param MISO
	 * @param MOSI
	 * @param SCK
	 */
	SPI(int ch,GPIO_TypeDef*GPIO_SPI,int MISO,int MOSI,int SCK);
	/**
	 * @brief SPI configuration
	 *
	 * Essential to use SPI
	 * @param mode
	 * @param dir
	 * @param div
	 * @param SPI_Datasize_Bitwidth
	 */
	void Config(SPI_MODE mode,SPI_DIR dir,SPI_CLKDIV div,int SPI_Datasize_Bitwidth);
	/**
	 *
	 * @param data
	 */
	void Write(unsigned char data);
	/**
	 *
	 * @param data
	 */
	void Write(uint16_t data);
	/*
	 *
	 *
	 */
	uint16_t getLatest(void);
	void AddHandeler(handler_t handler,void* arg);
	void IRQ(void);
	int Begin();
	void End();
	bool isBusy(void);
};
/*! @}*/
#endif /* SPI_H_ */
