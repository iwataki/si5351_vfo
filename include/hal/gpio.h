/*
 * gpio.h
 *
 *  Created on: 2014/12/28
 *      Author: 岩滝　宗一郎
 */

#ifndef GPIO_H_
#define GPIO_H_
#include <stm32f30x_conf.h>
/*! @addtogroup Peripheral */
/*! @{*/
/**
 *
 * @brief デジタル1ビット入力
 */
class DigitalIn{
private:
	GPIO_TypeDef*port;
	int pin;
public:
	/**
	 * @brief デジタル入力初期化
	 * @param port GPIO
	 * @param pin GPIO_Pin_x
	 * @param RCC_ch RCC_AHB1_GPIOx
	 */
	DigitalIn(GPIO_TypeDef*port,int pin,int RCC_ch);
	/**
	 *	@brief 入力を読む
	 * @return High=true,Low=false
	 */
	bool Read(void);
};
/**
 *
 * @brief デジタル1ビット出力
 */
class DigitalOut{
private:
	GPIO_TypeDef*port;
	int pin;
public:
	/**
	 * @brief デジタル出力初期化
	 * @param port GPIOx
	 * @param pin GPIO_Pin_x
	 * @param RCC_ch RCC_AHB_GPIOx
	 */
	DigitalOut(GPIO_TypeDef*port,int pin,int RCC_ch);
	/**
	 * @brief 出力を設定
	 * @param val High=true,Low=false
	 */
	void Write(bool val);
};
/*! @}*/
#endif /* GPIO_H_ */
