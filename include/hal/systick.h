/*
 * systick.h
 *
 *  Created on: 2012/09/16
 *      Author: ���@�@��Y
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_
/*
 * period=us
 *
 */
#ifdef __cplusplus
extern "C"{
#endif

void init_systick(int period,void(*func)(void));
void systick_change_handler(void(*func)(void));
void delay(int ms);

#ifdef __cplusplus
}
#endif
#endif /* SYSTICK_H_ */
