/*
 * uart.h
 *
 *  Created on: 2012/09/28
 *      Author: 岩滝　宗一郎
 */

#ifndef UART_H_
#define UART_H_

class Uart{
private:
	static const int BUFF_SIZE=1024;
	typedef struct ringbuff{
		int write_p,read_p,state;
		char data[BUFF_SIZE];
	} ringbuff_t;//ソフトウエアリングバッファ
	typedef struct ringbuff_dma{
		int read_ptr;
		char data[BUFF_SIZE];
	} ringbuff_dma_t;//dmaによる受信バッファ
	void gpio_init_for_uart(GPIO_TypeDef*GPIOx,uint16_t txpin,uint16_t rxpin);
	void usartmodule_init(USART_TypeDef*USARTx,uint32_t brr);//initialize usart
	void dma_init_for_uart(DMA_Channel_TypeDef*DMA_Channel,USART_TypeDef*usartx,uint32_t buffaddr);
	void nvic_init_for_uart(uint8_t NVIC_IRQChannel);//nvic initialize
	ringbuff_t txbuf;
	ringbuff_dma_t rxbuf;
	DMA_Channel_TypeDef*dma_channel;
	USART_TypeDef*usart_channel;
	int ch;
public:
	enum Baud{
		br2400=0x3a98,
		br9600=0x0ea6,
		br19200=0x0753,
		br57600=0x0271,
		br115200=0x138,
		br230400=0x09c,
		br460800=0x04e,
		br921600=0x027,
		br2250000=0x010,
		br4600000=0x008
	};
	Uart(int channel,Baud br);
	char getc();
	void putc(char c);
	bool is_available();
	char getc_noblock();
	virtual ~Uart();
	void interrupt_handler();
	static Uart*Uart1_interrupt;
	static Uart*Uart2_interrupt;
	static Uart*Uart3_interrupt;
};

enum uart_brr{
	br2400=0x3a98,
	br9600=0x0ea6,
	br19200=0x0753,
	br57600=0x0271,
	br115200=0x138,
	br230400=0x09c,
	br460800=0x04e,
	br921600=0x027,
	br2250000=0x010,
	br4600000=0x008,//36MHzでは不可
};//ビットレート

#endif /* UART_H_ */
