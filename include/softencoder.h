#ifndef __SOFTENC_H__
//IOポートから低速のロータリエンコーダを読む
#define __SOFTENC_H__
//A相B相のつながっているピンを指定
#include "hal/gpio.h"

void init_enc(int dirc);
void set_encoder_val(int val);
int get_encoder_val(void);
void update_encoder(void);

class SoftRotaryEnc{
private:
	DigitalIn&_phaseA;
	DigitalIn&_phaseB;
	bool _invert;
	int _count;
	const int enc_table[16]={0,-1,1,2,1,0,2,-1,-1,2,0,1,2,1,-1,0};
	int prev_portval;
	int prev_d;
public:
	SoftRotaryEnc(DigitalIn&phaseA,DigitalIn&phaseB,bool invert=false);
	void SetValue(int val);
	int GetValue(void);
	void Update(void);
};

#endif
