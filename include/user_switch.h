/*
 * user_switch.h
 *
 *  Created on: 2016/11/01
 *      Author: �@��Y
 */

#ifndef USER_SWITCH_H_
#define USER_SWITCH_H_

#include "hal/gpio.h"

void init_usersw(void);
void update_switch(void);
int get_switch(int);
class UserSwitch{
private:
	DigitalIn&_pin;
	bool _prev_pushed;
	bool _now_pused;
public:
	UserSwitch(DigitalIn&pin);
	void update();
	bool pushed();
	bool released();
};

#endif /* USER_SWITCH_H_ */
