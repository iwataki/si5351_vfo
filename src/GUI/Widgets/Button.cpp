/*
 * Button.cpp
 *
 *  Created on: 2017/04/03
 *      Author: 宗一郎
 */

#include "GUI/Widgets/Button.h"

Button::Button(Adafruit_GFX*graphic_driver,int x,int y,int width,int height,const std::string&str) :
Textbox(graphic_driver,x,y,width,height,str),
_pushed(false)
{
	needRedraw(true);
	// TODO Auto-generated constructor stub

}
void Button::drawNotPushed(void){
	_graphic_device->drawFastHLine(_x,_y,_width,_upColor);
	_graphic_device->drawFastVLine(_x,_y,_height,_upColor);
	_graphic_device->drawFastHLine(_x,_y+_height,_width,_bottomColor);
	_graphic_device->drawFastVLine(_x+_width,_y,_height,_bottomColor);
}
void Button::drawPushed(void){
	_graphic_device->drawFastHLine(_x,_y,_width,_bottomColor);
	_graphic_device->drawFastVLine(_x,_y,_height,_bottomColor);
	_graphic_device->drawFastHLine(_x,_y+_height,_width,_upColor);
	_graphic_device->drawFastVLine(_x+_width,_y,_height,_upColor);
}
void Button::draw(void){
	Textbox::draw();
	//枠をつける
	if(_pushed){
		drawPushed();
	}else{
		drawNotPushed();
	}
}
void Button::setState(bool pushed){
	_pushed=pushed;
	needRedraw(true);
}
Button::~Button() {

}

