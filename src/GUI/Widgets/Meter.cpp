/*
 * Meter.cpp
 *
 *  Created on: 2017/04/03
 *      Author: �@��Y
 */

#include "GUI/Widgets/Meter.h"

Meter::Meter(Adafruit_GFX*graphic_driver,int x,int y,int width,int height):
Widget(graphic_driver,x,y,width,height),
_max(10),
_min(0),
_value(0){
	// TODO Auto-generated constructor stub

}
void Meter::setMax(int max){
	_max=max;
	needRedraw(true);
}
void Meter::setMin(int min){
	_min=min;
	needRedraw(true);
}
void Meter::setValue(int val){
	_value=val;
	needRedraw(true);
}
Meter::~Meter() {
	// TODO Auto-generated destructor stub
}

