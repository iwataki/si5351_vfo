/*
 * ProgressBar.cpp
 *
 *  Created on: 2017/04/08
 *      Author: �@��Y
 */

#include "GUI/Widgets/ProgressBar.h"

ProgressBar::ProgressBar(Adafruit_GFX*graphic_driver,int x,int y,int width,int height):
Meter(graphic_driver,x,y,width,height),
_barColor(0){
	// TODO Auto-generated constructor stub

}
void ProgressBar::draw(void){
	erase();
	needRedraw(false);
	int barLengthPixcel=_width*(_value-_min)/(_max-_min);
	if(barLengthPixcel>_width){
		barLengthPixcel=_width;
	}
	_graphic_device->drawRect(_x,_y,_width,_height,_upColor);
	_graphic_device->fillRect(_x,_y,barLengthPixcel,_height,_barColor);
}
void ProgressBar::setBarColor(uint16_t barcolor){
	_barColor=barcolor;
	needRedraw(true);
}
ProgressBar::~ProgressBar() {
	// TODO Auto-generated destructor stub
}

