/*
 * Textbox.cpp
 *
 *  Created on: 2017/04/03
 *      Author: �@��Y
 */

#include "GUI/Widgets/Textbox.h"

Textbox::Textbox(Adafruit_GFX*graphic_driver,int x,int y,int width,int height,const std::string&str):
Widget(graphic_driver,x,y,width,height),
_str(str),
_wrap(false),
_txtColor(0){
	draw();
}
void Textbox::draw(void){
	needRedraw(false);
	erase();
	uint8_t size=_height/8;
	int string_width=5*size*_str.length();
	_graphic_device->setTextColor(_txtColor);
	_graphic_device->setCursor(_x+(_width-string_width)/2,_y+(_height%8)/2);
	_graphic_device->setTextWrap(_wrap);

	_graphic_device->setTextSize(size);
	_graphic_device->print(_str);
}
void Textbox::setText(const std::string&str){
	_str=str;
	needRedraw(true);
}
void Textbox::setTextColor(uint16_t color){
	_txtColor=color;
	needRedraw(true);
}
Textbox::~Textbox() {
	// TODO Auto-generated destructor stub
}

