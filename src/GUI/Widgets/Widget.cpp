/*
 * Widgets.cpp
 *
 *  Created on: 2017/04/03
 *      Author: �@��Y
 */

#include "GUI/Widgets/Widget.h"
#include "GUI/Widgets/WindowManager.h"

Widget::Widget(Adafruit_GFX*graphic_driver,int x,int y,int width,int height):
_graphic_device(graphic_driver),
_x(x),
_y(y),
_width(width),
_height(height),
_cb(0),
_user_data(0),
_bgColor(0),
_bottomColor(0),
_upColor(0),
_is_changed(false)
{


}
void Widget::setBoundColor(uint16_t up,uint16_t bottom){
	_upColor=up;
	_bottomColor=bottom;
}
void Widget::setBackGroundColor(uint16_t color){
	_bgColor=color;
}
void Widget::callback(callback_t cb,void*user_data){
	_cb=cb;
	_user_data=user_data;
}
void Widget::erase(void){
	_graphic_device->fillRect(_x,_y,_width,_height,_bgColor);
}
void Widget::remove(void){
	WindowManager::getInstance().removeWidget(this);
}
bool Widget::needRedraw(void){
	return _is_changed;
}
void Widget::needRedraw(bool is_requested){
	_is_changed=is_requested;
}
Widget::~Widget() {
	// TODO Auto-generated destructor stub
}

