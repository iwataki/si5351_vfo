/*
 * Window.cpp
 *
 *  Created on: 2017/04/12
 *      Author: �@��Y
 */

#include <GUI/Widgets/Window.h>


Window::Window(Adafruit_GFX*display,int width,int height):
Widget(display,0,0,width,height){

}
void Window::draw(void){
	for(auto pItem=_Items.begin();pItem!=_Items.end();pItem++){
		if((*pItem)->needRedraw())
			(*pItem)->draw();
	}
	needRedraw(false);
}
Window::~Window() {
	for(auto pItem=_Items.begin();pItem!=_Items.end();pItem++){
		delete(*pItem);
	}
}

