/*
 * WindowManager.cpp
 *
 *  Created on: 2017/04/05
 *      Author: �@��Y
 */

#include "gui/Widgets/WindowManager.h"
#include "hal/InterruptCriticalSection.h"

WindowManager::WindowManager() :
_isrunning(false),
_issynced(false),
_init_func(0),
_loop_func(0),
_display(0),
_widgets(){


}
WindowManager WindowManager::_instance;
WindowManager&WindowManager::getInstance(){
	//static WindowManager instance;
	return _instance;
}
void WindowManager::addWidget(Widget*w){
	_widgets.push_back(w);

}
void WindowManager::removeWidget(Widget*w){
	for(std::vector<Widget*>::iterator itr=_widgets.begin();itr!=_widgets.end();){
		if(*itr==w){
			itr=_widgets.erase(itr);
			delete w;
			continue;
		}else{
			itr++;
		}
	}
}
void WindowManager::run(){
	if(_init_func){
		_init_func();
	}
	_isrunning=true;
	while(1){
		if(_loop_func){
			_loop_func();
		}
		for(std::vector<Widget*>::iterator pwidget_itr=_widgets.begin();pwidget_itr<_widgets.end();pwidget_itr++){
			if((*pwidget_itr)->needRedraw()){
				(*pwidget_itr)->draw();
			}
		}
	}
}
void WindowManager::addDisplay(Adafruit_GFX*display){
	_display=display;
}
Adafruit_GFX*WindowManager::getDisplay(void){
	return _display;
}
bool WindowManager::isRunnning(void){
	return _isrunning;
}
void WindowManager::sync(void){
	_issynced=true;
}

void WindowManager::syncWait(void){
	if(_issynced){
		_issynced=false;
		return;
	}
	while(!_issynced);
	_issynced=false;
}
void WindowManager::registerInit(void(*func)(void)){
	_init_func=func;
}

void WindowManager::registerLoop(void(*func)(void)){
	_loop_func=func;
}
WindowManager::~WindowManager() {
	for(std::vector<Widget*>::iterator pwidget_itr=_widgets.begin();pwidget_itr<_widgets.end();pwidget_itr++){
		delete *pwidget_itr;
	}

}

