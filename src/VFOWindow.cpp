/*
 * VFOWinfow.cpp
 *
 *  Created on: 2017/04/12
 *      Author: �@��Y
 */

#include <VFOWindow.h>
#include "graphicDriver/Adafruit_ST7735.h"

VFOWindow::VFOWindow(Adafruit_GFX*display) :
		Window(display, ST7735_TFTHEIGHT_18, ST7735_TFTWIDTH) {
	setBackGroundColor(ST7735_BLUE);
	setBoundColor(ST7735_CYAN, Adafruit_ST7735::Color565(0x00, 0x00, 0x80));
	_leftBtn=new Button(display,0,_height-13,_width/3-1,12);
	_Items.push_back(_leftBtn);
	_leftBtn->setText("Func 1");
	_leftBtn->setTextColor(ST7735_GREEN);

	_centerBtn=new Button(display,_width/3,_height-13,_width/3-1,12);
	_Items.push_back(_centerBtn);
	_centerBtn->setText("RIT");
	_centerBtn->setTextColor(ST7735_GREEN);

	_rightBtn=new Button(display,_width*2/3,_height-13,_width/3-1,12);
	_Items.push_back(_rightBtn);
	_rightBtn->setText("STEP");
	_rightBtn->setTextColor(ST7735_GREEN);

	_freqencyText=new Textbox(display,0,10,_width,20);
	_Items.push_back(_freqencyText);
	_freqencyText->setText("50.000000");
	_freqencyText->setTextColor(ST7735_WHITE);

	_frequencyStepText=new Textbox(display,0,40,_width/2,12);
	_Items.push_back(_frequencyStepText);
	_frequencyStepText->setText("STEP=100Hz");
	_frequencyStepText->setTextColor(ST7735_YELLOW);

	_ritStatusText=new Textbox(display,0,52,_width/2,12);
	_Items.push_back(_ritStatusText);
	_ritStatusText->setText("RIT OFF");
	_ritStatusText->setTextColor(ST7735_YELLOW);

	_ritFreqText=new Textbox(display,_width/2,52,_width/2,12);
	_Items.push_back(_ritFreqText);
	_ritFreqText->setText("RIT=0Hz");
	_ritFreqText->setTextColor(_bgColor);

	_vfoStatusText=new Textbox(display,0,64,_width/2,12);
	_Items.push_back(_vfoStatusText);
	_vfoStatusText->setText("VFO A");
	_vfoStatusText->setTextColor(ST7735_YELLOW);

	_operationModeText=new Textbox(display,0,76,_width/2,12);
	_Items.push_back(_operationModeText);
	_operationModeText->setText("SSB");
	_operationModeText->setTextColor(ST7735_YELLOW);

	_xmitStatusText=new Textbox(display,_width/2,76,_width/2,12);
	_Items.push_back(_xmitStatusText);
	_xmitStatusText->setText("RX");
	_xmitStatusText->setTextColor(ST7735_YELLOW);

	for(auto item=_Items.begin();item!=_Items.end();item++){
		(*item)->setBackGroundColor(_bgColor);
		(*item)->setBoundColor(_upColor,_bottomColor);
	}
	needRedraw(true);
}
void VFOWindow::setFreq(int freq) {
	int MHz=freq/1000000;
	int KHz=freq%1000000;
	char str[16];
	sprintf(str,"%2d.%06d",MHz,KHz);
	_freqencyText->setText(str);
	needRedraw(true);
}
void VFOWindow::setVfoAB(int ch) {
	if(ch ==0){
		_vfoStatusText->setText("VFO A");
	}else{
		_vfoStatusText->setText("VFO B");
	}
	needRedraw(true);
}
void VFOWindow::setFreqStep(int step) {
	char str[16];

	sprintf(str,"STEP=%dHz",step);
	_frequencyStepText->setText(str);
	needRedraw(true);
}
void VFOWindow::setRitFocus(bool focused){
	if(focused){
		_ritStatusText->setBackGroundColor(ST7735_CYAN);
	}else{
		_ritStatusText->setBackGroundColor(_bgColor);
	}
	needRedraw(true);
}
void VFOWindow::setRitEnable(bool enable) {
	if(enable){
		_ritStatusText->setText("RIT ON");
		_ritFreqText->setTextColor(ST7735_YELLOW);
	}else{
		_ritStatusText->setText("RIT OFF");
		_ritFreqText->setTextColor(_bgColor);
	}
	needRedraw(true);
}
void VFOWindow::setLeftButton(bool pushed) {
	_leftBtn->setState(pushed);
	needRedraw(true);
}
void VFOWindow::setCenterButton(bool pushed) {
	_centerBtn->setState(pushed);
	needRedraw(true);
}
void VFOWindow::setRitFreq(int freq){
	char str[16];

	sprintf(str,"RIT=%dHz",freq);
	_ritFreqText->setText(str);
	needRedraw(true);
}
void VFOWindow::setRightButton(bool pushed) {
	_rightBtn->setState(pushed);
	needRedraw(true);
}
void VFOWindow::setFunction(int func){
	switch(func){
	case 0:
		_leftBtn->setText("Func 1");
		_centerBtn->setText("RIT");
		_rightBtn->setText("F.Step");
		break;
	case 1:
		_leftBtn->setText("Func 2");
		_centerBtn->setText("VFO A/B");
		_rightBtn->setText("MODE");
		break;
	default:
		break;
	}
	needRedraw(true);
}
void VFOWindow::setModurationMode(int mode){
	if(mode==0){
		_operationModeText->setText("SSB");
	}else{
		_operationModeText->setText("AM");
	}
	needRedraw(true);
}
void VFOWindow::setTransmitStatus(bool txing){
	if(txing){
		_xmitStatusText->setText("TX");
		_xmitStatusText->setBackGroundColor(ST7735_MAGENTA);
		_xmitStatusText->setTextColor(ST7735_WHITE);
	}else{
		_xmitStatusText->setText("RX");
		_xmitStatusText->setBackGroundColor(_bgColor);
		_xmitStatusText->setTextColor(ST7735_YELLOW);
	}
	needRedraw(true);
}
VFOWindow::~VFOWindow() {
	// TODO Auto-generated destructor stub
}

