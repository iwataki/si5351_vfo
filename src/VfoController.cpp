/*
 * VfoController.cpp
 *
 *  Created on: 2017/05/03
 *      Author: 宗一郎
 */

#include "VfoController.h"

VfoController::VfoController(VFOWindow&_disp,AD9834&_dds,SoftRotaryEnc&_enc,UserSwitch&_leftbutton,UserSwitch&_centerButton,UserSwitch&_rightButton,UserSwitch&_pttsw,DigitalOut&_setAM):
	display(&_disp),
	dds(&_dds),
	encoder(&_enc),
	leftButton(&_leftbutton),
	centerButton(&_centerButton),
	rightButton(&_rightButton),
	pttSwitch(&_pttsw),
	SetSSBMode(&_setAM),
	FreqencyStep(100),
	activeVfoChannel(0),
	IF_value(9000000+28000000),
	function(0),
	enc_prevcount(0),
	ritstatus(rit_disable),
	RitFreq(0),
	transmitting(false),
	moduration_mode(mod_ssb){
		SetSSBMode->Write(true);
		Frequency[0]=50000000;
		Frequency[1]=50000000;
		dds->SetFrequency(Frequency[0]-IF_value);
}
void VfoController::change_ritstatus(void){
	switch(ritstatus){
	case rit_disable:
		ritstatus=rit_setfreq;
		display->setRitEnable(true);
		display->setRitFocus(true);
		break;
	case rit_enable:
		ritstatus=rit_disable;
		display->setRitEnable(false);
		break;
	case rit_setfreq:
		ritstatus=rit_enable;
		display->setRitEnable(true);
		display->setRitFocus(false);
		break;
	default:
		break;
	}

}
void VfoController::OnKnobRotate(int diff){
	if(ritstatus==rit_setfreq){
		RitFreq+=10*diff;
		if(RitFreq>2500){
			RitFreq=2500;
		}else if(RitFreq<-2500){
			RitFreq=-2500;
		}
		display->setRitFreq(RitFreq);
	}else{
		int temp_freq=Frequency[activeVfoChannel];
		temp_freq+=FreqencyStep*diff;
		if(temp_freq>MaxFreqency){
			temp_freq=MaxFreqency;
		}
		if(temp_freq<MinFreqency){
			temp_freq=MinFreqency;
		}
		Frequency[activeVfoChannel]=temp_freq;
		display->setFreq(temp_freq);
	}
	FreqUpdate();
}
void VfoController::FreqUpdate(){
	int freq=Frequency[activeVfoChannel];
	if(ritstatus!=rit_disable&&!transmitting){//受信中でありrit有効
		freq+=RitFreq;
	}
	freq-=IF_value;
	dds->SetFrequency(freq);
}
void VfoController::hwupdate(void){
	encoder->Update();
	leftButton->update();
	centerButton->update();
	rightButton->update();
	pttSwitch->update();
}
void VfoController::Update(void){
	hwupdate();
	int enc_val=encoder->GetValue();
	int enc_diff=enc_val-enc_prevcount;
	enc_prevcount=enc_val;
	if(enc_diff!=0){
		OnKnobRotate(enc_diff);
	}
	if(pttSwitch->pushed()){
		display->setTransmitStatus(true);
		transmitting=true;
		FreqUpdate();
	}
	if(pttSwitch->released()){
		display->setTransmitStatus(false);
		transmitting=false;
		FreqUpdate();
	}

	if(leftButton->pushed()){//change function 1<->2
		display->setLeftButton(true);
		if(function==0){
			function=1;
		}else{
			function=0;
		}
		display->setFunction(function);
	}
	if(leftButton->released()){
		display->setLeftButton(false);
	}
	if(centerButton->pushed()){//rit control @ func1, VFO A/B ctrl @ func2
		display->setCenterButton(true);
		if(function==0){
			change_ritstatus();
		}else{//vfo a/b change
			if(activeVfoChannel==0){
				activeVfoChannel=1;
			}else{
				activeVfoChannel=0;
			}
			display->setVfoAB(activeVfoChannel);
			display->setFreq(Frequency[activeVfoChannel]);
		}
		FreqUpdate();
	}
	if(centerButton->released()){
		display->setCenterButton(false);
	}
	if(rightButton->pushed()){
		display->setRightButton(true);
		if(function==0){//change step
			switch(FreqencyStep){
			case 10:
				FreqencyStep=100;
				break;
			case 100:
				FreqencyStep=1000;
				break;
			case 1000:
				FreqencyStep=10;
				break;
			default:
				FreqencyStep=10;
				break;
			}
			display->setFreqStep(FreqencyStep);
		}else{
			if(moduration_mode==mod_ssb){
				moduration_mode=mod_am;
				SetSSBMode->Write(false);
			}else{
				moduration_mode=mod_ssb;
				SetSSBMode->Write(true);
			}
			display->setModurationMode(moduration_mode);
		}
	}
	if(rightButton->released()){
		display->setRightButton(false);
	}
}

VfoController::~VfoController() {
	// TODO Auto-generated destructor stub
}

