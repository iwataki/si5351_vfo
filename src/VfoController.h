/*
 * VfoController.h
 *
 *  Created on: 2017/05/03
 *      Author: �@��Y
 */

#ifndef VFOCONTROLLER_H_
#define VFOCONTROLLER_H_
#include "ad9834.h"
#include "softencoder.h"
#include "user_switch.h"
#include "hal/gpio.h"
#include "VFOWindow.h"
class VfoController {
private:
	VFOWindow*display;
	AD9834*dds;
	SoftRotaryEnc*encoder;
	UserSwitch*leftButton;
	UserSwitch*centerButton;
	UserSwitch*rightButton;
	UserSwitch*pttSwitch;
	DigitalOut*SetSSBMode;
	int Frequency[2];
	int activeVfoChannel;
	const int MaxFreqency=54000000;
	const int MinFreqency=50000000;
	int IF_value;
	int FreqencyStep;
	int RitFreq;
	int function;
	int enc_prevcount;
	enum ritstate{rit_disable,rit_enable,rit_setfreq};
	int ritstatus;
	enum moduration{mod_ssb,mod_am};
	int moduration_mode;
	bool transmitting;
	void hwupdate(void);
	void change_ritstatus(void);
	void OnKnobRotate(int diff);
	void FreqUpdate();
public:
	VfoController(VFOWindow&_disp,AD9834&_dds,SoftRotaryEnc&_enc,UserSwitch&_leftbutton,UserSwitch&_centerButton,UserSwitch&_rightButton,UserSwitch&_pttsw,DigitalOut&_setAM);
	void Update(void);
	virtual ~VfoController();
};

#endif /* VFOCONTROLLER_H_ */
