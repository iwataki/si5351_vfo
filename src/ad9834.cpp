/*
 * AD9834.c
 *
 *  Created on: 2016/10/08
 *      Author: �@��Y
 */


#include "ad9834.h"

AD9834::AD9834(SPI&Port,DigitalOut&CS,DigitalOut&Reset):
	_Port(Port),
	_CS(CS),
	_Reset(Reset){
	_CS.Write(true);
	_Reset.Write(true);
	_Port.Config(SPI::Mode2,SPI::Master,SPI::div8,SPI_DataSize_16b);
	reset();
}
void AD9834::SetFrequency(uint32_t Freq){
	uint64_t temp=((uint64_t)Freq<<28)/AD9834REFCLK;
	uint32_t dfWord=temp&0x0fffffff;
	uint16_t freq_msb=(dfWord>>14)&0x3fff;
	uint16_t freq_lsb=dfWord&0x3fff;
	sendWord(0x2000);
	sendWord(0x4000|freq_lsb);
	sendWord(0x4000|freq_msb);
}
void AD9834::sendWord(uint16_t data){
	_Port.Begin();
	_CS.Write(false);
	_Port.Write(data);
	while(_Port.isBusy());
	_CS.Write(true);
	_Port.End();
}
void AD9834::reset(void){
	_Reset.Write(true);
	_Reset.Write(false);
	sendWord(0x0000);
}
