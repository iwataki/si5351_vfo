/*
 * InterruptCriticalSection.cpp
 *
 *  Created on: 2017/03/26
 *      Author: �@��Y
 */

#include "hal/InterruptCriticalSection.h"
#include "stm32f30x_conf.h"

InterruptCriticalSection::InterruptCriticalSection() {
	__disable_irq();

}

InterruptCriticalSection::~InterruptCriticalSection() {
	__enable_irq();
}

