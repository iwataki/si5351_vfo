/*
 * gpio.cpp
 *
 *  Created on: 2014/12/28
 *      Author: ���@�@��Y
 */
#include "hal/gpio.h"

DigitalIn::DigitalIn(GPIO_TypeDef*port,int pin,int RCC_ch){
	this->port=port;
	this->pin=pin;
	RCC_AHBPeriphClockCmd(RCC_ch,ENABLE);
	GPIO_InitTypeDef init;
	GPIO_StructInit(&init);
	init.GPIO_Mode=GPIO_Mode_IN;
	init.GPIO_PuPd=GPIO_PuPd_UP;
	init.GPIO_Speed=GPIO_Speed_50MHz;
	init.GPIO_Pin=pin;
	GPIO_Init(this->port,&init);
}
bool DigitalIn::Read(void){
	if(GPIO_ReadInputDataBit(this->port,this->pin)==Bit_SET){
		return true;
	}else{
		return false;
	}
}

DigitalOut::DigitalOut(GPIO_TypeDef*port,int pin,int RCC_ch){
	this->port=port;
	this->pin=pin;
	RCC_AHBPeriphClockCmd(RCC_ch,ENABLE);
	GPIO_InitTypeDef init;
	GPIO_StructInit(&init);
	init.GPIO_Mode=GPIO_Mode_OUT;
	init.GPIO_OType=GPIO_OType_PP;
	init.GPIO_Speed=GPIO_Speed_50MHz;
	init.GPIO_Pin=pin;
	GPIO_Init(this->port,&init);
}
void DigitalOut::Write(bool val){
	if(val){
		GPIO_WriteBit(this->port,this->pin,Bit_SET);
	}else{
		GPIO_WriteBit(this->port,this->pin,Bit_RESET);
	}
}



