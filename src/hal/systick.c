/*
 * systick.c
 *
 *  Created on: 2012/09/16
 *      Author: 岩滝　宗一郎
 */

#include "stm32f30x_conf.h"
#include "hal/systick.h"
void(*systick_handler_proc)(void);
/*! \brief 周期割り込みを発生させる
 *
 *  \param int period 周期（us）
 *  \param func 呼び出されるべき関数ポインタ
 */
volatile int _period;
volatile int _lpcout;
void init_systick(int period,void(*func)(void)){//period:in us
	systick_handler_proc=func;
	_period=period;
	NVIC_InitTypeDef nvic_initstr;
	NVIC_SetPriorityGrouping(NVIC_PriorityGroup_3);
	nvic_initstr.NVIC_IRQChannel=SysTick_IRQn;
	nvic_initstr.NVIC_IRQChannelCmd=ENABLE;
	nvic_initstr.NVIC_IRQChannelPreemptionPriority=0;
	nvic_initstr.NVIC_IRQChannelSubPriority=0;
	NVIC_Init(&nvic_initstr);
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);//9MHz
	SysTick_Config(SystemCoreClock/1000000*period);
}
void systick_change_handler(void(*func)(void)){
	systick_handler_proc=func;
}
void delay(int ms){
	_lpcout=ms*1000/_period;
	while(_lpcout);
}
/*
 * 割り込みハンドラ
 */
void SysTick_Handler(void);
void SysTick_Handler(void){
	if(_lpcout>0){
		_lpcout--;
	}
	systick_handler_proc();
}
