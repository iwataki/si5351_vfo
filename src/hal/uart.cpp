/*
 * uart.c
 *
 *  Created on: 2012/09/28
 *      Author: 岩滝　宗一郎
 */

#include "stm32f30x_conf.h"
#include "hal/uart.h"
#include "hal/InterruptCriticalSection.h"

#define USART_SENDING 1
#define USART_NOT_SENDING 0
/*
 * USARTようにGPIOを初期化する
 *
 */
//initialize gpio for usart
void Uart::gpio_init_for_uart(GPIO_TypeDef*GPIOx,uint16_t txpin,uint16_t rxpin){
	GPIO_InitTypeDef gpioinitstr;
	gpioinitstr.GPIO_Mode=GPIO_Mode_AF;//TX Pin
	gpioinitstr.GPIO_OType=GPIO_OType_PP;
	gpioinitstr.GPIO_Pin=txpin;
	gpioinitstr.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOx,&gpioinitstr);
	gpioinitstr.GPIO_Mode=GPIO_Mode_AF;//RX Pin
	gpioinitstr.GPIO_PuPd=GPIO_PuPd_UP;
	gpioinitstr.GPIO_Pin=rxpin;
	GPIO_Init(GPIOx,&gpioinitstr);
}
/*
 * USART本体を初期化
 */

void Uart::usartmodule_init(USART_TypeDef*USARTx,uint32_t brr){
	USART_InitTypeDef usartinitstr;
	usartinitstr.USART_BaudRate=brr;//通信パラメータ設定
	usartinitstr.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
	usartinitstr.USART_Parity=USART_Parity_No;
	usartinitstr.USART_StopBits=USART_StopBits_1;
	usartinitstr.USART_WordLength=USART_WordLength_8b;
	usartinitstr.USART_Mode=USART_Mode_Rx|USART_Mode_Tx;

	USART_Init(USARTx,&usartinitstr);
}
/*
 * DMAを初期化
 */
//initialize dma
void Uart::dma_init_for_uart(DMA_Channel_TypeDef*DMA_Channel,USART_TypeDef*usartx,uint32_t buffaddr){
	DMA_InitTypeDef dmainitstr;
	dmainitstr.DMA_PeripheralBaseAddr=(uint32_t)(&usartx->RDR);
	dmainitstr.DMA_PeripheralDataSize=DMA_PeripheralDataSize_Byte;
	dmainitstr.DMA_PeripheralInc=DMA_PeripheralInc_Disable;
	dmainitstr.DMA_DIR=DMA_DIR_PeripheralSRC;
	dmainitstr.DMA_BufferSize=BUFF_SIZE;
	dmainitstr.DMA_MemoryBaseAddr=buffaddr;
	dmainitstr.DMA_MemoryDataSize=DMA_MemoryDataSize_Byte;
	dmainitstr.DMA_MemoryInc=DMA_MemoryInc_Enable;
	dmainitstr.DMA_Mode=DMA_Mode_Circular;
	dmainitstr.DMA_M2M=DMA_M2M_Disable;
	dmainitstr.DMA_Priority=DMA_Priority_Medium;
	DMA_Init(DMA_Channel,&dmainitstr);
}
/*
 * 割り込みの設定
 */

void Uart::nvic_init_for_uart(uint8_t NVIC_IRQChannel){
	NVIC_InitTypeDef nvicinitstr;
	nvicinitstr.NVIC_IRQChannel=NVIC_IRQChannel;
	nvicinitstr.NVIC_IRQChannelCmd=ENABLE;
	nvicinitstr.NVIC_IRQChannelPreemptionPriority=3;
	nvicinitstr.NVIC_IRQChannelSubPriority=0;
	NVIC_Init(&nvicinitstr);
}

/*
 * USART初期化
 * ch:1,2,3
 * brr:baudrate
 */
Uart::Uart(int ch,Baud brr){
	NVIC_SetVectorTable(NVIC_VectTab_FLASH,0x000);//DFUファーム分，割り込みベクタを移動
	NVIC_SetPriorityGrouping(NVIC_PriorityGroup_3);
	this->ch=ch;
	switch (ch){
	case 1:
		//GPIO設定
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);//GPIO clock設定
		gpio_init_for_uart(GPIOA,GPIO_Pin_9,GPIO_Pin_10);//GPIO設定
		//USART設定
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);//usartモジュールにクロック供給
		USART_Cmd(USART1,ENABLE);//usart有効
		usartmodule_init(USART1,brr);//usart設定
		//USART_ITConfig(USART1,USART_IT_TXE,ENABLE);//送信完了割り込み
		//DMA設定(受信バッファ)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,ENABLE);//DMA clk
		dma_init_for_uart(DMA1_Channel5,USART1,(uint32_t)rxbuf.data);//DMA init
		DMA_Cmd(DMA1_Channel5,ENABLE);
		USART_DMACmd(USART1,USART_DMAReq_Rx,ENABLE);//ENABLE DMA
		//NVIC設定

		nvic_init_for_uart(USART1_IRQn);//ENABLE irqn
		usart_channel=USART1;
		dma_channel=DMA1_Channel5;
		Uart1_interrupt=this;
		break;

	case 2:
		//GPIO設定
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);//GPIO clock設定
		gpio_init_for_uart(GPIOA,GPIO_Pin_2,GPIO_Pin_3);//GPIO設定
		//USART設定
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);//usartモジュールにクロック供給
		USART_Cmd(USART2,ENABLE);//usart有効
		usartmodule_init(USART2,brr);//usart設定
		//USART_ITConfig(USART2,USART_IT_TXE,ENABLE);//送信完了割り込み
		//DMA設定(受信バッファ)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,ENABLE);//DMA clk
		dma_init_for_uart(DMA1_Channel6,USART2,(uint32_t)rxbuf.data);//DMA init
		DMA_Cmd(DMA1_Channel6,ENABLE);
		USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);//ENABLE DMA
		//NVIC設定
		nvic_init_for_uart(USART2_IRQn);//ENABLE irqn

		usart_channel=USART2;
		dma_channel=DMA1_Channel6;
		Uart2_interrupt=this;
		break;
	case 3:
		//GPIO設定
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB,ENABLE);//GPIO clock設定
		gpio_init_for_uart(GPIOB,GPIO_Pin_10,GPIO_Pin_11);//GPIO設定
		//USART設定
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);//usartモジュールにクロック供給
		USART_Cmd(USART3,ENABLE);//usart有効
		usartmodule_init(USART3,brr);//usart設定
		//USART_ITConfig(USART3,USART_IT_TXE,ENABLE);//送信完了割り込み
		//DMA設定(受信バッファ)
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,ENABLE);//DMA clk
		dma_init_for_uart(DMA1_Channel3,USART3,(uint32_t)rxbuf.data);//DMA init
		DMA_Cmd(DMA1_Channel3,ENABLE);
		USART_DMACmd(USART3,USART_DMAReq_Rx,ENABLE);//ENABLE DMA
		//NVIC設定
		nvic_init_for_uart(USART3_IRQn);//ENABLE irqn

		usart_channel=USART3;
		dma_channel=DMA1_Channel3;
		Uart3_interrupt=this;
		break;
	default:
		break;
	}
	//バッファ初期化
	rxbuf.read_ptr=0;
	txbuf.state=USART_NOT_SENDING;
	txbuf.read_p=0;
	txbuf.write_p=0;
}
Uart*Uart::Uart1_interrupt=0;
Uart*Uart::Uart2_interrupt=0;
Uart*Uart::Uart3_interrupt=0;
Uart::~Uart(){
switch(this->ch){
case 1:
	Uart1_interrupt=0;
	break;
case 2:
	Uart2_interrupt=0;
	break;
case 3:
	Uart3_interrupt=0;
	break;
}
}
/*
 * 1文字送信
 */
void Uart::putc(char c){
	InterruptCriticalSection lock;
	if(txbuf.state==USART_NOT_SENDING){
		//atomic
		USART_SendData(usart_channel,(uint16_t)c);
		USART_ITConfig(usart_channel,USART_IT_TXE,ENABLE);//送信完了割り込み許可
		txbuf.state=USART_SENDING;//送信中にする
		}else{
			txbuf.data[txbuf.write_p]=c;
			txbuf.write_p++;
			txbuf.write_p&=BUFF_SIZE-1;
		}
}
/*
 * １文字受信
 */
char Uart::getc_noblock(void){
	char c=0;
	if(rxbuf.read_ptr!=BUFF_SIZE-dma_channel->CNDTR){//データあり
		__disable_irq();
		c=rxbuf.data[rxbuf.read_ptr];
		rxbuf.read_ptr++;
		rxbuf.read_ptr&=BUFF_SIZE-1;
		__enable_irq();
	}
	return c;//データがない場合は0を返す.
}
/*
 * 受信確認
 * 受信＝１
 */
bool Uart::is_available(void){
	if(rxbuf.read_ptr!=BUFF_SIZE-dma_channel->CNDTR){
		return true;
	}else{
		return false;
	}
}
/*
 * １文字受信，受信してない場合は待つ
 */
char Uart::getc(void){
	char c;
	while(rxbuf.read_ptr==BUFF_SIZE-dma_channel->CNDTR);
	InterruptCriticalSection lock;
	c=rxbuf.data[rxbuf.read_ptr];
	rxbuf.read_ptr++;
	rxbuf.read_ptr&=BUFF_SIZE-1;
	return c;
}
void Uart::interrupt_handler(){
	char c;
	if(USART_GetITStatus(usart_channel,USART_IT_TXE)){
		if(txbuf.read_p==txbuf.write_p){
			USART_ITConfig(usart_channel,USART_IT_TXE,DISABLE);//バッファに送るデータがなくなった
			txbuf.state=USART_NOT_SENDING;
		}else{
			c=txbuf.data[txbuf.read_p];
			USART_SendData(USART1,(uint16_t)c);
			txbuf.write_p++;
			txbuf.write_p&=BUFF_SIZE-1;
		}
	}
}
/*
 * USART割り込み
 */
extern "C"{
void USART1_IRQHandler(void){//USART1割り込み処理
	if(Uart::Uart1_interrupt){
		Uart::Uart1_interrupt->interrupt_handler();
	}
}

void USART2_IRQHandler(void){//USART2割り込み処理
	if(Uart::Uart2_interrupt){
		Uart::Uart2_interrupt->interrupt_handler();
	}
}

void USART3_IRQHandler(void){//USART3割り込み処理
	if(Uart::Uart3_interrupt){
		Uart::Uart3_interrupt->interrupt_handler();
	}
}
}
