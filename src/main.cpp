/*
 * main.c
 *
 *  Created on: 2017/03/26
 *      Author: �@��Y
 */

#include <stdint.h>
#include "system_stm32f30x.h"
#include "stm32f30x_conf.h"
#include "hal/systick.h"
#include "hal/SPI.h"
#include "hal/gpio.h"
#include "graphicDriver/Adafruit_ST7735.h"
#include "VFOWindow.h"
#include "GUI/Widgets/WindowManager.h"
#include <string>
#include "ad9834.h"
#include "softencoder.h"
#include "user_switch.h"
#include "VfoController.h"

VFOWindow*vfoWindow=0;
VfoController*controller=0;

void tick(void){
	WindowManager::getInstance().sync();
}
void initdisplay(void){

}
void loop(void){
	WindowManager::getInstance().syncWait();
	controller->Update();
}
int main(){
	init_systick(10000,tick);
	DigitalOut lcdrst(GPIOA,GPIO_Pin_0,RCC_AHBPeriph_GPIOA);
	DigitalOut lcdcs(GPIOA,GPIO_Pin_1,RCC_AHBPeriph_GPIOA);
	DigitalOut rs(GPIOA,GPIO_Pin_2,RCC_AHBPeriph_GPIOA);
	DigitalOut ddsreset(GPIOA,GPIO_Pin_3,RCC_AHBPeriph_GPIOA);
	DigitalOut ddscs(GPIOA,GPIO_Pin_4,RCC_AHBPeriph_GPIOA);
	SPI spi(1,GPIOA,6,7,5);
	SPI spidds(1,GPIOA,6,7,5);
	AD9834 dds(spidds,ddscs,ddsreset);
	ddscs.Write(true);

	DigitalIn lbtn(GPIOB,GPIO_Pin_7,RCC_AHBPeriph_GPIOB);
	DigitalIn cbtn(GPIOB,GPIO_Pin_6,RCC_AHBPeriph_GPIOB);
	DigitalIn rbtn(GPIOB,GPIO_Pin_5,RCC_AHBPeriph_GPIOB);
	UserSwitch leftsw(lbtn);
	UserSwitch centersw(cbtn);
	UserSwitch rightsw(rbtn);

	DigitalIn enca(GPIOB,GPIO_Pin_4,RCC_AHBPeriph_GPIOB);
	DigitalIn encb(GPIOB,GPIO_Pin_3,RCC_AHBPeriph_GPIOB);
	SoftRotaryEnc enc(enca,encb,true);

	DigitalIn ptt(GPIOA,GPIO_Pin_15,RCC_AHBPeriph_GPIOA);
	UserSwitch pttsw(ptt);
	DigitalOut modulation(GPIOA,GPIO_Pin_14,RCC_AHBPeriph_GPIOA);

	Adafruit_ST7735 lcd(spi,lcdcs,rs,lcdrst);
	lcd.initR(INITR_18BLACKTAB);
	lcd.setRotation(1);
	lcd.fillScreen(ST7735_BLUE);
	vfoWindow=new VFOWindow(&lcd);
	controller=new VfoController(*vfoWindow,dds,enc,leftsw,centersw,rightsw,pttsw,modulation);
	WindowManager::getInstance().addWidget(vfoWindow);
	WindowManager::getInstance().addDisplay(&lcd);
	WindowManager::getInstance().registerInit(initdisplay);
	WindowManager::getInstance().registerLoop(loop);
	WindowManager::getInstance().run();
	return 0;
}
