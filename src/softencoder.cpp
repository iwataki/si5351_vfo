/*
 * softencoder.c
 *
 *  Created on: 2012/03/25
 *      Author: ���@�@��Y
 */

#include "softencoder.h"

SoftRotaryEnc::SoftRotaryEnc(DigitalIn&phaseA,DigitalIn&phaseB,bool invert):
	_phaseA(phaseA),
	_phaseB(phaseB),
	_invert(invert),
	_count(0),
	prev_portval(0),
	prev_d(0){

}

void SoftRotaryEnc::SetValue(int val){
	_count=val;
}
int SoftRotaryEnc::GetValue(void){
	return _count;
}

void SoftRotaryEnc::Update(void){
	int portval=0;
	int dir=_invert?(-1):1;

	portval=(_phaseA.Read()?0x02:0)|(_phaseB.Read()?0x01:0);
	if(enc_table[portval<<2|prev_portval]==2){
		_count+=dir*prev_d*2;
	}else{
		_count+=dir*enc_table[portval<<2|prev_portval];
		prev_d=enc_table[portval<<2|prev_portval];
	}
	prev_portval=portval;
}
