/*
 * user_switch.c
 *
 *  Created on: 2016/11/01
 *      Author: �@��Y
 */

#include "user_switch.h"

UserSwitch::UserSwitch(DigitalIn&pin):
	_pin(pin),
	_prev_pushed(false),
	_now_pused(false){

}
void UserSwitch::update(){
	_prev_pushed=_now_pused;
	_now_pused=!_pin.Read();
}
bool UserSwitch::pushed(){
	return _now_pused&&!_prev_pushed;
}
bool UserSwitch::released(){
	return !_now_pused&&_prev_pushed;
}
